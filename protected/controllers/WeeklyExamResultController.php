<?php

class WeeklyExamResultController extends Controller
{

	public $layout='//layouts/column3';

	public function actionIndex()
	{
		$this->render('index');
	}

	// Uncomment the following methods and override them if needed
public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/* array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			), */
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>"isset(Yii::app()->user->group_id)&& Yii::app()->user->group_id==1",
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
}