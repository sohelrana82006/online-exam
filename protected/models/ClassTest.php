<?php

/**
 * This is the model class for table "class_test".
 *
 * The followings are the available columns in table 'class_test':
 * @property integer $id
 * @property string $class_name
 * @property integer $module_id
 * @property integer $course_id
 * @property string $date
 */
class ClassTest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ClassTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'class_test';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('class_name, module_id, course_id, creat_date', 'required'),
			array('module_id, course_id', 'numerical', 'integerOnly'=>true),
			array('class_name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, class_name, module_id, course_id, creat_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'class_name' => 'Class Name',
			'module_id' => 'Module',
			'course_id' => 'Course',
			'creat_date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('class_name',$this->class_name,true);
		$criteria->compare('module_id',$this->module_id);
		$criteria->compare('course_id',$this->course_id);
		$criteria->compare('creat_date',$this->creat_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}