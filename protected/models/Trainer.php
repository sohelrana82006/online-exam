<?php

/**
 * This is the model class for table "trainer".
 *
 * The followings are the available columns in table 'trainer':
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $address
 * @property string $phone
 * @property string $picture
 * @property integer $tsp_id
 * @property integer $group_id
 */
class Trainer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Trainer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trainer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id, tsp_id, name, username, password, address, phone, picture', 'required'),
			array('group_id, tsp_id', 'numerical', 'integerOnly'=>true),
			array('name, username, password, phone, picture', 'length', 'max'=>100),
			array('address', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, group_id, tsp_id name, username, password, address, phone, picture', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'group_rel'=>array(self::BELONGS_TO,'Group','group_id'),
		'tsp_rel'=>array(self::BELONGS_TO,'TraningCenter','tsp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'tsp_id' => 'Tsp',
			'name' => 'Name',
			'username' => 'Username',
			'password' => 'Password',
			'address' => 'Address',
			'phone' => 'Phone',
			'picture' => 'Picture',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->with = array('group_rel');
		$criteria->with = array('tsp_rel');
		$criteria->compare('group_rel.group_name', $this->group_id, true );
		$criteria->compare('tsp_rel.tsp_name', $this->tsp_id, true );
		$criteria->compare('tsp_id',$this->tsp_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('picture',$this->picture,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}