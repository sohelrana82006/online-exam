<?php

/**
 * This is the model class for table "question".
 *
 * The followings are the available columns in table 'question':
 * @property integer $id
 * @property string $questions
 * @property string $correct_answer
 * @property integer $class_id
 * @property integer $module_id
 */
class Question extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Question the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'question';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('questions, correct_answer, batch_id, tsp_id, class_id', 'required'),
			array('batch_id, tsp_id, class_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, questions, correct_answer, batch_id, tsp_id, class_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'batch_rel'=>array(self::BELONGS_TO,'Batch','batch_id'),
		'tsp_rel'=>array(self::BELONGS_TO,'TraningCenter','tsp_id'),
		'class_rel'=>array(self::BELONGS_TO,'ClassTest','class_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'questions' => 'Questions',
			'correct_answer' => 'Correct Answer',
			'batch_id' => 'Batch',
			'tsp_id' => 'Tsp',
			'class_id' => 'Class',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('questions',$this->questions,true);
		$criteria->compare('correct_answer',$this->correct_answer,true);
		$criteria->with = array('batch_rel');
		$criteria->with = array('tsp_rel');
		$criteria->with = array('class_rel');
		$criteria->compare('batch_rel.batch_name', $this->batch_id, true );
		$criteria->compare('tsp_rel.tsp_name', $this->tsp_id, true );
		$criteria->compare('class_rel.class_name', $this->class_id, true );
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}