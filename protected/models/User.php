<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $user_name
 * @property integer $user_id
 * @property integer $tsp_id
 * @property integer $batch_id
 * @property string $address
 * @property string $phone
 * @property string $picture
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id, batch_id, tsp_id, course_id, name,  username, password,   address, phone', 'required'),
			array('photo', 'file', 'types'=>'jpg, gif, png','allowEmpty'=>true),
			array('group_id, course_id, tsp_id, batch_id', 'numerical', 'integerOnly'=>true),
			array('name, username,password, phone, photo', 'length', 'max'=>100),
			array('address', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, group_id, batch_id, tsp_id, course_id, name, username, password, address, phone, photo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'group_rel'=>array(self::BELONGS_TO,'Group','group_id'),
		'batch_rel'=>array(self::BELONGS_TO,'Batch','batch_id'),
		'tsp_rel'=>array(self::BELONGS_TO,'TraningCenter','tsp_id'),
		'course_rel'=>array(self::BELONGS_TO,'Course','course_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'batch_id' => 'Batch',
			'tsp_id' => 'Tsp',
			'course_id' => 'Course',
			'name' => 'Name',
			'username' => 'User Name',
			'password' => 'Password',
			'address' => 'Address',
			'phone' => 'Phone',
			'photo' => 'Photo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->with = array('group_rel');
		$criteria->compare('group_rel.group_name', $this->group_id, true );
		$criteria->with = array('batch_rel');
		$criteria->compare('batch_rel.batch_name', $this->batch_id, true );
		$criteria->with = array('tsp_rel');
		$criteria->compare('tsp_rel.tsp_name', $this->tsp_id, true );
		$criteria->with = array('course_rel');
		$criteria->compare('course_rel.course_name', $this->course_id, true );
		$criteria->compare('name',$this->name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('photo',$this->photo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}