<?php

/**
 * This is the model class for table "result".
 *
 * The followings are the available columns in table 'result':
 * @property integer $id
 * @property string $username
 * @property integer $batch_id
 * @property integer $tsp_id
 * @property integer $course_id
 * @property integer $module_id
 * @property integer $class_id
 * @property string $marks
 * @property string $exam_marks
 * @property string $update_date
 */
class Result extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Result the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'result';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, batch_id, tsp_id, course_id, module_id, class_id, marks, exam_marks, update_date', 'required'),
			array('batch_id, tsp_id, course_id, module_id, class_id', 'numerical', 'integerOnly'=>true),
			array('username, marks, exam_marks', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, batch_id, tsp_id, course_id, module_id, class_id, marks, exam_marks, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'batch_id' => 'Batch',
			'tsp_id' => 'Tsp',
			'course_id' => 'Course',
			'module_id' => 'Module',
			'class_id' => 'Class',
			'marks' => 'Marks',
			'exam_marks' => 'Exam Marks',
			'update_date' => 'Update Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('batch_id',$this->batch_id);
		$criteria->compare('tsp_id',$this->tsp_id);
		$criteria->compare('course_id',$this->course_id);
		$criteria->compare('module_id',$this->module_id);
		$criteria->compare('class_id',$this->class_id);
		$criteria->compare('marks',$this->marks,true);
		$criteria->compare('exam_marks',$this->exam_marks,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}