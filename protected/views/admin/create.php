<?php
/* @var $this AdminController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	'Create',
);
?>

<h1>Create Admin</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>