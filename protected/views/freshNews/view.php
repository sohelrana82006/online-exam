<?php
/* @var $this FreshNewsController */
/* @var $model FreshNews */

$this->breadcrumbs=array(
	'Fresh News'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List FreshNews', 'url'=>array('index')),
	array('label'=>'Create FreshNews', 'url'=>array('create')),
	array('label'=>'Update FreshNews', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FreshNews', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FreshNews', 'url'=>array('admin')),
);
?>

<h1>View FreshNews #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'dec',
		'creat_date',
	),
)); ?>
