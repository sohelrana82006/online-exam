<?php
/* @var $this FreshNewsController */
/* @var $model FreshNews */

$this->breadcrumbs=array(
	'Fresh News'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FreshNews', 'url'=>array('index')),
	array('label'=>'Create FreshNews', 'url'=>array('create')),
	array('label'=>'View FreshNews', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FreshNews', 'url'=>array('admin')),
);
?>

<h1>Update FreshNews <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>