<?php
/* @var $this FreshNewsController */
/* @var $model FreshNews */

$this->breadcrumbs=array(
	'Fresh News'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FreshNews', 'url'=>array('index')),
	array('label'=>'Manage FreshNews', 'url'=>array('admin')),
);
?>

<h1>Create FreshNews</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>