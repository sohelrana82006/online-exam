<!DOCTYPE html>
<html lang="en">
<head>
<title>Student's Site</title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.4.2.min.js" ></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/cufon-replace.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/Myriad_Pro_300.font.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/Myriad_Pro_400.font.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/1/js-image-slider.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/1/js-image-slider.js" type="text/javascript"></script>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/generic.css" rel="stylesheet" type="text/css" />
	
<!--[if lt IE 7]>
<link rel="stylesheet" href="css/ie6.css" type="text/css" media="screen">
<script type="text/javascript" src="js/ie_png.js"></script>
<script type="text/javascript">ie_png.fix('.png, footer, header nav ul li a, .nav-bg, .list li img');</script>
<![endif]-->
<!--[if lt IE 9]><script type="text/javascript" src="js/html5.js"></script><![endif]-->
</head>

<body id="page1">
<!-- START PAGE SOURCE -->
<div class="wrap">
  <header>
    <div class="container">
      <h1><a href="#">ONLINE EXAM</a></h1>
      <div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Examinee', 'url'=>array('/site/exam')),
				array('label'=>'Technical Contact ', 'url'=>array('/site/loginTsp')),
				array('label'=>'Trainer', 'url'=>array('/site/loginTrainer')),
				array('label'=>'Admin', 'url'=>array('/site/login')),
				array('label'=>'Registration', 'url'=>array('/site/registration')),
				//array('label'=>'LogIn', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); 
		
		/* $this->widget('ext.CDropDownMenu.CDropDownMenu',array(
            'style' => 'vertical', // or default or navbar
      'items'=>array(
                array(
                    'label'=>Yii::t('Login'),
                    'url'=>array('//user/user/login'),
                    'visible'=>Yii::app()->user->isGuest,
                    ), 
                array(
                    'label'=>Yii::t('Register'),
                    'url'=>array('//registration/registration/registration'),
                    'visible'=>Yii::app()->user->isGuest,
                    ), 
                array(
                    'label'=>Yii::t('Demo'),
                    'url'=>array('//demo/index'),
                    'visible'=>Yii::app()->user->isGuest,
                    ), 
                array(
                    'label'=>Yii::t('Demo'),
                    'visible'=>!Yii::app()->user->isGuest,
                    'items' => array(
                        array(
                            'label'=>Yii::t('Browse demos'),
                            'url'=>array('//demo/index'),
                            ), 
                        array(
                            'label'=>Yii::t('Create new Demo'),
                            'url'=>array('//demo/create'),
                            ), 
                        array(
                            'label'=>Yii::t('Demos'),
                            'url'=>array('//demo/index', 'owner' => true),
                            ), 
    ),
                array('label'=>'Logout ('.Yii::app()->user->name.')',
                        'url'=>array('/site/logout'),
                        'visible'=>!Yii::app()->user->isGuest)
                    )
    ) 
)
); */

/* $this->widget('application.extensions.superfish.RSuperfish', array(
  'items'=> array(
      array('label' => 'menu item', 'url' => '#', 'items' => array(
      array('label' => 'menu item', 'url' => '#'),
      array('label' => 'menu item', 'url' => '#', 'items' => array(
        array('label' => 'menu item', 'url' => '#')
      )),
          array('label' => 'menu item', 'url' => '#'),
          array('label' => 'menu item', 'url' => '#')
       )),
      array('label' => 'menu item', 'url' => '#'),
      array('label' => 'menu item', 'url' => '#'),
))); */
		
		?>
    </div>
    </div>
  </header>


<?php echo $content; ?>
<footer>
  <div class="footerlink">
    <p class="lf">Copyright &copy; 2010 <a href="#">SiteName</a> - All Rights Reserved</p>
    <p class="rf"><a href="http://www.free-css.com/">Free CSS Templates</a> by <a href="http://www.templatemonster.com/">TemplateMonster</a></p>
    <div style="clear:both;"></div>
  </div>
</footer>
<script type="text/javascript"> Cufon.now(); </script>
<!-- END PAGE SOURCE -->
</body>
</html>
