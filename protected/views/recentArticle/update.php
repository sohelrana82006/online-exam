<?php
/* @var $this RecentArticleController */
/* @var $model RecentArticle */

$this->breadcrumbs=array(
	'Recent Articles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RecentArticle', 'url'=>array('index')),
	array('label'=>'Create RecentArticle', 'url'=>array('create')),
	array('label'=>'View RecentArticle', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage RecentArticle', 'url'=>array('admin')),
);
?>

<h1>Update RecentArticle <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>