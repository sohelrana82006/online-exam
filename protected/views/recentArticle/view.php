<?php
/* @var $this RecentArticleController */
/* @var $model RecentArticle */

$this->breadcrumbs=array(
	'Recent Articles'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List RecentArticle', 'url'=>array('index')),
	array('label'=>'Create RecentArticle', 'url'=>array('create')),
	array('label'=>'Update RecentArticle', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete RecentArticle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RecentArticle', 'url'=>array('admin')),
);
?>

<h1>View RecentArticle #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'totle',
		'dec',
	),
)); ?>
