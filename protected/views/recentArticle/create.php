<?php
/* @var $this RecentArticleController */
/* @var $model RecentArticle */

$this->breadcrumbs=array(
	'Recent Articles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RecentArticle', 'url'=>array('index')),
	array('label'=>'Manage RecentArticle', 'url'=>array('admin')),
);
?>

<h1>Create RecentArticle</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>