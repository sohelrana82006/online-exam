<?php
/* @var $this RecentArticleController */
/* @var $model RecentArticle */

$this->breadcrumbs=array(
	'Recent Articles'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List RecentArticle', 'url'=>array('index')),
	array('label'=>'Create RecentArticle', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#recent-article-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Recent Articles</h1>

 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'recent-article-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'totle',
		'dec',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
