<?php
/* @var $this TrainerController */
/* @var $model Trainer */

$this->breadcrumbs=array(
	'Trainers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Trainer', 'url'=>array('index')),
	array('label'=>'Create Trainer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#trainer-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Trainers</h1>

 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trainer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
           'name'=>'group_id', 
           'value'=>'CHtml::encode($data->group_rel->group_name)', 
		    ),
		array(
           'name'=>'tsp_id', 
           'value'=>'CHtml::encode($data->tsp_rel->tsp_name)', 
		    ),
		'name',
		'username',
		'password',
		'address',
		'phone',
		'picture',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
