<?php
/* @var $this TrainerController */
/* @var $model Trainer */

$this->breadcrumbs=array(
	'Trainers'=>array('index'),
	'Create',
);
?>

<h1>Create Trainer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>