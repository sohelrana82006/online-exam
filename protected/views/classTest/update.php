<?php
/* @var $this ClassTestController */
/* @var $model ClassTest */

$this->breadcrumbs=array(
	'Class Tests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ClassTest', 'url'=>array('index')),
	array('label'=>'Create ClassTest', 'url'=>array('create')),
	array('label'=>'View ClassTest', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ClassTest', 'url'=>array('admin')),
);
?>

<h1>Update ClassTest <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>