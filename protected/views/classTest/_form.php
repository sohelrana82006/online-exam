<?php
/* @var $this ClassTestController */
/* @var $model ClassTest */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'class-test-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'class_name'); ?>
		<?php echo $form->textField($model,'class_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'class_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'module_id'); ?>
		<?php echo $form->dropDownList($model,'module_id', CHtml::listData(
	     Module::model()->findAll(array('order'=>'id')),'id','module_name')); ?>
		<?php echo $form->error($model,'module_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'course_id'); ?>
		<?php echo $form->dropDownList($model,'course_id', CHtml::listData(
	     Course::model()->findAll(array('order'=>'id')),'id','course_name')); ?>
		<?php echo $form->error($model,'course_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'creat_date'); ?>
		<?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'name' => 'ClassTest[creat_date]',
                'value'=> $model->creat_date,
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    //'altFormat' => 'yy-mm-dd', // show to user format
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'focus',
                    'buttonImageOnly' => false,
                ),
                'htmlOptions'=>array(
                    'class'=>'span5',
                ),
            ));
            ?>
		<?php echo $form->error($model,'creat_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->