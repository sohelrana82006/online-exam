<?php
/* @var $this ClassTestController */
/* @var $model ClassTest */

$this->breadcrumbs=array(
	'Class Tests'=>array('index'),
	'Create',
);
?>

<h1>Create ClassTest</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>