<?php
/* @var $this ClassTestController */
/* @var $model ClassTest */

$this->breadcrumbs=array(
	'Class Tests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ClassTest', 'url'=>array('index')),
	array('label'=>'Create ClassTest', 'url'=>array('create')),
	array('label'=>'Update ClassTest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ClassTest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ClassTest', 'url'=>array('admin')),
);
?>

<h1>View ClassTest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'class_name',
		'module_id',
		'course_id',
		'creat_date',
	),
)); ?>
