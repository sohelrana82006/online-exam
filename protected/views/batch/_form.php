<?php
/* @var $this BatchController */
/* @var $model Batch */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'batch-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_name'); ?>
		<?php echo $form->textField($model,'batch_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'batch_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'creat_date'); ?>
		<?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'name' => 'Batch[creat_date]',
                'value'=> $model->creat_date,
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    //'altFormat' => 'yy-mm-dd', // show to user format
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showOn' => 'focus',
                    'buttonImageOnly' => false,
                ),
                'htmlOptions'=>array(
                    'class'=>'span5',
                ),
            ));
            ?>
		<?php echo $form->error($model,'creat_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->