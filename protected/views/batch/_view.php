<?php
/* @var $this BatchController */
/* @var $data Batch */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_name')); ?>:</b>
	<?php echo CHtml::encode($data->batch_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('creat_date')); ?>:</b>
	<?php echo CHtml::encode($data->creat_date); ?>
	<br />


</div>