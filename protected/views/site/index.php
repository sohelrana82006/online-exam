<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<script> 
$(document).ready(function(){
  $("#flip1").mouseenter(function(){
    $("#panel1").slideDown("slow");
  });
});
  
$(document).ready(function(){
  $("#flip1").click(function(){
    $("#panel1").slideUp("slow");
  });
});



$(document).ready(function(){
  $("#flip2").mouseenter(function(){
    $("#panel2").slideDown("slow");
  });
});
  
$(document).ready(function(){
  $("#flip2").click(function(){
    $("#panel2").slideUp("slow");
  });
});




$(document).ready(function(){
  $("#flip3").mouseenter(function(){
    $("#panel3").slideDown("slow");
  });
});
  
$(document).ready(function(){
  $("#flip3").click(function(){
    $("#panel3").slideUp("slow");
  });
});

</script>
 
<style type="text/css"> 
#panel1,#flip1,#panel2,#flip2,#panel3,#flip3
{
padding:1px;
text-align:center;
color:#145287;
background-color:#DEDEDE;
border:solid 1px #c3c3c3;
border-radius:3px 3px 3px 3px ;
}
#panel1,#panel2,#panel3
{
padding:10px;
display:none;
}
</style>


   <div class="container">
    <aside>
      <h2 style="padding-left:40px">Fresh <span align="center">News</span></h2>
      <ul class="news">
	  <?php
	  $counter=0;
	  $news=FreshNews::model()->findAll();
	  foreach($news as $val):
	  $counter++;
	  ?>
        <li><div id="<?php echo 'flip'.$counter ?>"><b style="color:#ff0000"><?php echo $val->creat_date ?></b>
          <h4 style="color:#012548"><b><?php echo $val->title ?></b></h4></div>
         <div id="<?php echo 'panel'.$counter ?>"><?php echo $val->dec ?></div>
		 </li>
	  <?php endforeach;   ?>
      </ul>
    </aside>
    <section id="content">
          <div id="sliderFrame">
        <div id="slider">
            <a href="http://www.menucool.com/javascript-image-slider" target="_blank">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/images/image-slider-1.jpg" alt="Welcome to Menucool.com" />
            </a>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/images/image-slider-2.jpg" alt="" />
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/images/image-slider-3.jpg" alt="Pure Javascript. No jQuery. No flash." />
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/images/image-slider-4.jpg" alt="#htmlcaption" />
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/imageSlider/images/image-slider-5.jpg" />
        </div>
        <div id="htmlcaption" style="display: none;">
            <em>HTML</em> caption. Link to <a href="http://www.google.com/">Google</a>.
        </div>
    </div>
      <div class="inside">
        <h2>Recent <span>Articles</span></h2>
        <ul class="list">
		<?php 
		$counter=0;
		$article=RecentArticle::model()->findAll(array('limit'=>'3','order'=>'rand()'));
		foreach($article as $data):
		$counter++;
		?>
          <li><span><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/<?php echo 'icon'.$counter ?>.png"></span>
            <h4><?php echo $data->totle  ?></h4>
            <p><?php echo $data->dec  ?></p>
          </li>
		 <?php  endforeach;   ?>
        </ul>
        <h2>Move Forward <span>With Your Education</span></h2>
        <p><span class="txt1">Eusus consequam</span> vitae habitur amet nullam vitae condis phasellus sed justo. Orcivel mollis intesque eu sempor ridictum a non laorem lacingilla wisi. </p>
        <div class="img-box"><img src="images/1page-img.jpg">Eusus consequam vitae habitur amet nullam vitae condis phasellus sed justo. Orcivel mollis intesque eu sempor ridictum a non laorem lacingilla wisi. Nuncrhoncus eros <a href="#">maurien ulla</a> facilis tor mauris tincidunt et vitae morbi. Velelit condimentes in laorem quis nullamcorper nam fauctor feugiat pellent sociis.</div>
        <p class="p0">Eusus consequam vitae habitur amet nullam vitae condis phasellus sed justo. Orcivel mollis intesque eu sempor ridictum a <a href="#">non laorem</a> lacingilla wisi.</p>
      </div>
    </section>
  </div>
