<div class="form" style="padding-left:50px;">

<?php
$model=new Question;




 $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-form',
	'enableAjaxValidation'=>false,
	'action'=>array('site/question'),
)); 
?>
	<?php echo "<h2>Select Catagory</h2>"; ?>
	<div class="row">
		<?php echo $form->labelEx($model,'batch'); ?>
		<?php echo $form->dropDownList($model,'batch_id', CHtml::listData(
	     batch::model()->findAll(array('order'=>'id')),'id','batch_name')); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'question_limit'); ?>
		<?php echo $form->textField($model,'question_limit'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->