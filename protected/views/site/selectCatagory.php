<div class="form" style="padding-left:50px;">

<?php
$model=new Question;




 $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-form',
	'enableAjaxValidation'=>false,
	'action'=>array('site/question'),
)); 
?>
	<?php echo "<h2>Select Catagory</h2>"; ?>
	<?php 
	$id=yii::app()->user->Id;
	$user = User::model()->findAll("id=$id");
	
	foreach($user as $data):
		$batch_id=$data->batch_id;
		$tsp_id=$data->tsp_id;
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'batch'); ?>
		<?php echo $form->dropDownList($model,'batch_id', CHtml::listData(
	     Batch::model()->findAll(array('order'=>'id','condition'=>"id=$batch_id")),'id','batch_name')); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tsp'); ?>
		<?php echo $form->dropDownList($model,'tsp_id', CHtml::listData(
	     TraningCenter::model()->findAll(array('order'=>'id','condition'=>"id=$tsp_id")),'id','tsp_name')); ?>
	</div>
	
	<?php endforeach ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'class'); ?>
		<?php echo $form->dropDownList($model,'class_id', CHtml::listData(
	     ClassTest::model()->findAll(array('order'=>'id')),'id','class_name')); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


