<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Update Question', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Question', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	
);
?>

<h1>View Question #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'questions',
		'correct_answer',
		array(
           'name'=>'batch_id', 
           'value'=>CHtml::encode($model->batch_rel->batch_name), 
		    ),
		array(
           'name'=>'tsp_id', 
           'value'=>CHtml::encode($model->tsp_rel->tsp_name), 
		    ),
		array(
           'name'=>'class_id', 
           'value'=>CHtml::encode($model->class_rel->class_name), 
		    ),
	),
)); ?>
