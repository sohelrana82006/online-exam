<?php
/* @var $this QuestionController */
/* @var $model Question */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Question', 'url'=>array('index')),
	array('label'=>'Create Question', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#question-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Questions</h1>

 
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'question-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'questions',
		'correct_answer',
		array(
           'name'=>'batch_id', 
           'value'=>'CHtml::encode($data->batch_rel->batch_name)', 
		    ),
		array(
           'name'=>'tsp_id', 
           'value'=>'CHtml::encode($data->tsp_rel->tsp_name)', 
		    ),
			array(
           'name'=>'class_id', 
           'value'=>'CHtml::encode($data->class_rel->class_name)', 
		    ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
