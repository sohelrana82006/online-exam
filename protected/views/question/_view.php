<?php
/* @var $this QuestionController */
/* @var $data Question */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('questions')); ?>:</b>
	<?php echo CHtml::encode($data->questions); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correct_answer')); ?>:</b>
	<?php echo CHtml::encode($data->correct_answer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_rel->batch_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tsp_id')); ?>:</b>
	<?php echo CHtml::encode($data->tsp_rel->tsp_name); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('class_id')); ?>:</b>
	<?php echo CHtml::encode($data->class_rel->class_name); ?>
	<br />


</div>