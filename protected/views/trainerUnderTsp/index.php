<?php
/* @var $this TrainerUnderTspController */

$this->breadcrumbs=array(
	'Trainer Under Tsp',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<div class="form" style="padding-left:50px;">

<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	'action'=>array('trainerUnderTsp/trainerUnderTsp'),

));
 ?>
<h2 style="color:#145875;">Select Traning Center Name</h2>
	<div class="row">
		<?php echo $form->labelEx($model,'tsp_name'); ?>
		<?php echo $form->dropDownList($model,'id', CHtml::listData(
	     TraningCenter::model()->findAll(array('order'=>'id')),'id','tsp_name')); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Submit'); ?>
	</div>
	
<?php $this->endWidget(); ?>

</div><!-- form -->
