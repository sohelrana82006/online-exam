<?php
/* @var $this CourseUnderTspController */
/* @var $model CourseUnderTsp */

$this->breadcrumbs=array(
	'Course Under Tsps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CourseUnderTsp', 'url'=>array('index')),
	array('label'=>'Create CourseUnderTsp', 'url'=>array('create')),
	array('label'=>'View CourseUnderTsp', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CourseUnderTsp', 'url'=>array('admin')),
);
?>

<h1>Update CourseUnderTsp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>