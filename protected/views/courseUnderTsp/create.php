<?php
/* @var $this CourseUnderTspController */
/* @var $model CourseUnderTsp */

$this->breadcrumbs=array(
	'Course Under Tsps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CourseUnderTsp', 'url'=>array('index')),
	array('label'=>'Manage CourseUnderTsp', 'url'=>array('admin')),
);
?>

<h1>Create CourseUnderTsp</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>