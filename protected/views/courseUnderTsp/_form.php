<?php
/* @var $this CourseUnderTspController */
/* @var $model CourseUnderTsp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'course-under-tsp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'course_id'); ?>
		<?php echo $form->dropDownList($model,'course_id', CHtml::listData(
	     Course::model()->findAll(array('order'=>'id')),'id','course_name')); ?>
		<?php echo $form->error($model,'course_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tsp_id'); ?>
		<?php echo $form->dropDownList($model,'tsp_id', CHtml::listData(
	     TraningCenter::model()->findAll(array('order'=>'id')),'id','tsp_name')); ?>
		<?php echo $form->error($model,'tsp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model,'batch_id', CHtml::listData(
	     Batch::model()->findAll(array('order'=>'id')),'id','batch_name')); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trainer_id'); ?>
		<?php echo $form->dropDownList($model,'trainer_id', CHtml::listData(
	     Trainer::model()->findAll(array('order'=>'id')),'id','name')); ?>
		<?php echo $form->error($model,'trainer_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->