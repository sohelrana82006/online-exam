<?php
/* @var $this CourseUnderTspController */
/* @var $model CourseUnderTsp */

$this->breadcrumbs=array(
	'Course Under Tsps'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CourseUnderTsp', 'url'=>array('index')),
	array('label'=>'Create CourseUnderTsp', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#course-under-tsp-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Course Under Tsps</h1>

 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'course-under-tsp-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		 array(
           'name'=>'tsp_id', 
           'value'=>'CHtml::encode($data->tsp_rel->tsp_name)', 
		    ),
		 array(
           'name'=>'course_id', 
           'value'=>'CHtml::encode($data->course_rel->course_name)', 
		    ),
		 array(
           'name'=>'batch_id', 
           'value'=>'CHtml::encode($data->batch_rel->batch_name)', 
		    ),
		 array(
           'name'=>'trainer_id', 
           'value'=>'CHtml::encode($data->trainer_rel->name)', 
		    ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
