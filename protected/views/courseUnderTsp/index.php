<?php
/* @var $this CourseUnderTspController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Course Under Tsps',
);

$this->menu=array(
	array('label'=>'Create CourseUnderTsp', 'url'=>array('create')),
	array('label'=>'Manage CourseUnderTsp', 'url'=>array('admin')),
);
?>

<h1>Course Under Tsps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
