<?php
/* @var $this CourseUnderTspController */
/* @var $data CourseUnderTsp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('tsp_id')); ?>:</b>
	<?php echo CHtml::encode($data->tsp_rel->tsp_name); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('course_id')); ?>:</b>
	<?php echo CHtml::encode($data->course_rel->course_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($data->batch_rel->batch_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trainer_id')); ?>:</b>
	<?php echo CHtml::encode($data->trainer_rel->name); ?>
	<br />


</div>