<?php
/* @var $this CourseUnderTspController */
/* @var $model CourseUnderTsp */

$this->breadcrumbs=array(
	'Course Under Tsps'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CourseUnderTsp', 'url'=>array('index')),
	array('label'=>'Create CourseUnderTsp', 'url'=>array('create')),
	array('label'=>'Update CourseUnderTsp', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CourseUnderTsp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CourseUnderTsp', 'url'=>array('admin')),
);
?>

<h1>View CourseUnderTsp #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
           'name'=>'tsp_id', 
           'value'=>CHtml::encode($model->tsp_rel->tsp_name), 
		    ),
		array(
           'name'=>'course_id', 
           'value'=>CHtml::encode($model->course_rel->course_name), 
		    ),
		array(
           'name'=>'batch_id', 
           'value'=>CHtml::encode($model->batch_rel->batch_name), 
		    ),
		array(
           'name'=>'trainer_id', 
           'value'=>CHtml::encode($model->trainer_rel->name), 
		    ),
	),
)); ?>
