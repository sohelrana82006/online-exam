<?php
/* @var $this ResultController */
/* @var $model Result */

$this->breadcrumbs=array(
	'Results'=>array('index'),
	'Create',
);
?>

<h1>Create Result</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>