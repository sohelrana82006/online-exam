<?php
/* @var $this TraningCenterController */
/* @var $model TraningCenter */

$this->breadcrumbs=array(
	'Traning Centers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TraningCenter', 'url'=>array('index')),
	array('label'=>'Create TraningCenter', 'url'=>array('create')),
	array('label'=>'View TraningCenter', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TraningCenter', 'url'=>array('admin')),
);
?>

<h1>Update TraningCenter <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>