<?php
/* @var $this TraningCenterController */
/* @var $model TraningCenter */

$this->breadcrumbs=array(
	'Traning Centers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TraningCenter', 'url'=>array('index')),
	array('label'=>'Manage TraningCenter', 'url'=>array('admin')),
);
?>

<h1>Create TraningCenter</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>