<?php
/* @var $this TraningCenterController */
/* @var $model TraningCenter */

$this->breadcrumbs=array(
	'Traning Centers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TraningCenter', 'url'=>array('index')),
	array('label'=>'Create TraningCenter', 'url'=>array('create')),
	array('label'=>'Update TraningCenter', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TraningCenter', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TraningCenter', 'url'=>array('admin')),
);
?>

<h1>View TraningCenter #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tsp_name',
		'location',
	),
)); ?>
