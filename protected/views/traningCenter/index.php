<?php
/* @var $this TraningCenterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Traning Centers',
);

$this->menu=array(
	array('label'=>'Create TraningCenter', 'url'=>array('create')),
	array('label'=>'Manage TraningCenter', 'url'=>array('admin')),
);
?>

<h1>Traning Centers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
