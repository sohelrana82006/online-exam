<?php
/* @var $this TraningCenterController */
/* @var $model TraningCenter */

$this->breadcrumbs=array(
	'Traning Centers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TraningCenter', 'url'=>array('index')),
	array('label'=>'Create TraningCenter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#traning-center-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Traning Centers</h1>

 

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'traning-center-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tsp_name',
		'location',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
