<?php
/* @var $this TecnicalContactController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tecnical Contacts',
);

$this->menu=array(
	array('label'=>'Create TecnicalContact', 'url'=>array('create')),
	array('label'=>'Manage TecnicalContact', 'url'=>array('admin')),
);
?>

<h1>Tecnical Contacts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
