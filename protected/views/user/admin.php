<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		 array(
           'name'=>'group_id', 
           'value'=>'CHtml::encode($data->group_rel->group_name)', 
		    ),
		 array(
           'name'=>'batch_id', 
           'value'=>'CHtml::encode($data->batch_rel->batch_name)', 
		    ),
		 /* array(
           'name'=>'tsp_id', 
           'value'=>'CHtml::encode($data->tsp_rel->tsp_name)', 
		    ), */
		 array(
           'name'=>'course_id', 
           'value'=>'CHtml::encode($data->course_rel->course_name)', 
		    ),
		'name',
		'username',
		'password',
		'address',
		'phone',
		array(
		'name'=>'photo',
		'type'=>'html',
		'header'=>'photo',
		'value'=>'CHtml::image("upload/".$data->photo, "image", array("width"=>50))',
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
