<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<?php
	$group=Group::model()->findAll("id=4") ;
	foreach($group as $val):
	?>
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('group_id')); ?>:</b>
	<?php echo CHtml::encode($val->group_name); ?>
	<br />
	
	<?php endforeach; ?>
	
	<?php
	$batch=Batch::model()->findAll("id=$data->batch_id") ;
	foreach($batch as $val2):
	?>
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('batch_id')); ?>:</b>
	<?php echo CHtml::encode($val2->batch_name); ?>
	<br />
	
	<?php endforeach; ?>
	
	<?php
	$tsp=TraningCenter::model()->findAll("id=$data->tsp_id") ;
	foreach($tsp as $val3):
	?>
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('tsp_id')); ?>:</b>
	<?php echo CHtml::encode($val3->tsp_name); ?>
	<br />
	
	<?php endforeach; ?>
	
	<?php
	$course=Course::model()->findAll("id=$data->course_id") ;
	foreach($course as $val4):
	?>
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('course_id')); ?>:</b>
	<?php echo CHtml::encode($val4->course_name); ?>
	<br />
	
	<?php endforeach; ?>
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php echo CHtml::encode($data->address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('photo')); ?>:</b><br/>
	<img width="120" src="<?php echo Yii::app()->request->baseUrl ?>/upload/<?php echo $data->photo; ?>"/>
	<br />

</div>