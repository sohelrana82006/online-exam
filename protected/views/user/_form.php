<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form" style="padding-left:50px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'group_id'); ?>
		<?php echo $form->dropDownList($model,'group_id', CHtml::listData(
	     Group::model()->findAll(array('order'=>'id','condition'=>"id=4")),'id','group_name')); ?>
		<?php echo $form->error($model,'group_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model,'batch_id', CHtml::listData(
	     Batch::model()->findAll(array('order'=>'id')),'id','batch_name')); ?>
		<?php echo $form->error($model,'batch_id'); ?>
	</div>
	
	<?php 
	$id=yii::app()->user->Id;
	$user = TecnicalContact::model()->findAll("id=$id");
	foreach($user as $data):
		$tsp_id=$data->tsp_id;
	?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'tsp'); ?>
		<?php echo $form->dropDownList($model,'tsp_id', CHtml::listData(
	     TraningCenter::model()->findAll(array('order'=>'id','condition'=>"id=$tsp_id")),'id','tsp_name')); ?>
	</div>
	
	<?php endforeach ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'course_id'); ?>
		<?php echo $form->dropDownList($model,'course_id', CHtml::listData(
	     Course::model()->findAll(array('order'=>'id')),'id','course_name')); ?>
		<?php echo $form->error($model,'course_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->textField($model,'password',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

 <?php if($model->photo): ?>
		<div class="row">
			<label>  Already Uploaded  Photo: </label>
			 
	         <img width="150" src="<?php echo Yii::app()->request->baseUrl; ?>/upload/<?php echo $model->photo; ?>">
	</div>
     <?php endif; ?>
<br/>
    <div class="row">
		<?php echo $form->labelEx($model,'photo'); ?>
		<?php echo CHtml::activeFileField($model, 'photo'); ?>
		<?php echo $form->error($model,'photo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->